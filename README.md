To compile the application .Net 5 sdk should be installed (https://dotnet.microsoft.com/download)
1. In the command line go to the app folder (ShoppingBasketApp) 
2. Run **dotnet build**
3. Go to the build folder (on Windows '...\ShoppingBasketApp\ShoppingService\bin\Debug\net5.0\')
4. Run **dotnet <NameOfTheFile.dll>**
