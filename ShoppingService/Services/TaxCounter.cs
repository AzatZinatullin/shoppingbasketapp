﻿using ShoppingService.Models;
using System;
using System.Linq;

namespace ShoppingService.Services
{
    public static class TaxCounter
    {
        private static readonly string[] reducedTaxCategories =
            new string[] { "candy", "popcorn", "coffee" };

        private static readonly decimal basicSalesTax = 0.1m;
        private static readonly decimal importDuty = 0.05m;

        public static decimal CountProductTax(Product product)
        {
            var result = 0.0m;
            if (!reducedTaxCategories.Contains(product.Category))
            {
                result += RoundToOneTwentieth(product.Price * basicSalesTax);
            }

            if (product.IsImported)
            {
                result += RoundToOneTwentieth(product.Price * importDuty);
            }

            return result;
        }

        private static decimal RoundToOneTwentieth(decimal value) => Math.Ceiling(value * 20) / 20;
    }
}
