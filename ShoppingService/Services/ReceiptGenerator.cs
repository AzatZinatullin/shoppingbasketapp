﻿using ShoppingService.Models;

namespace ShoppingService.Services
{
    public static class ReceiptGenerator
    {
        public static Receipt GetReceipt(ShoppingBasket shoppingBasket)
        {
            var receipt = new Receipt();
            receipt.ReceiptId = shoppingBasket.BasketId;
            receipt.Goods = shoppingBasket.Goods;

            GetSalesTaxesAndCostOfGoods(shoppingBasket, out var salesTaxes, out var costOfGoods);

            receipt.SalesTaxes = salesTaxes;
            receipt.TotalPrice = costOfGoods + salesTaxes;
            return receipt;
        }

        private static void GetSalesTaxesAndCostOfGoods(ShoppingBasket shoppingBasket,
            out decimal salesTaxes, out decimal costOfGoods)
        {
            salesTaxes = 0;
            costOfGoods = 0;

            foreach (var product in shoppingBasket.Goods)
            {
                product.Tax = TaxCounter.CountProductTax(product);
                salesTaxes += product.Tax * product.Number;
                costOfGoods += product.Price * product.Number;
            }
        }
    }
}
