﻿using System.Collections.Generic;

namespace ShoppingService.Models
{
    public record ShoppingBasket
    {
        public int BasketId { get; set; }

        public List<Product> Goods { get; set; }
    }
}
