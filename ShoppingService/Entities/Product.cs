﻿namespace ShoppingService.Models
{
    public record Product
    {
        public string Name { get; set; }

        public string Category { get; set; }

        public bool IsImported { get; set; }

        public int Number { get; set; }

        public decimal Price { get; set; }

        public decimal Tax { get; set; }
    }
}
