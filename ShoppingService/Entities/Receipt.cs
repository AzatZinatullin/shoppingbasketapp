﻿using System.Collections.Generic;

namespace ShoppingService.Models
{
    public record Receipt
    {
        public int ReceiptId { get; set; }

        public List<Product> Goods { get; set; }

        public decimal SalesTaxes { get; set; }

        public decimal TotalPrice { get; set; }
    }
}
