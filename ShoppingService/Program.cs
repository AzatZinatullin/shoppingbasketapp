﻿using ShoppingService.Data;
using ShoppingService.Models;
using ShoppingService.Services;
using System;

namespace ShoppingService
{
    public class Program
    {
        public static void PrintReceipt(Receipt receipt)
        {
            var goods = String.Empty;
            foreach (var product in receipt.Goods)
            {
                goods += $"{product.Number} {product.Name}: {(product.Price + product.Tax) * product.Number}\n";
            }

            Console.WriteLine($"Receipt {receipt.ReceiptId}:\n" +
                $"{goods}" +
                $"Sales Taxes: {receipt.SalesTaxes}\n" +
                $"Total: {receipt.TotalPrice}\n");
        }

        static void Main(string[] args)
        {
            foreach (var shoppingBasket in InputData.GetDataInputFile())
            {
                PrintReceipt(ReceiptGenerator.GetReceipt(shoppingBasket));
            }
        }
    }
}
