﻿using ShoppingService.Models;
using System.Collections.Generic;

namespace ShoppingService.Data
{
    public static class InputData
    {
        public static IEnumerable<ShoppingBasket> GetDataInputFile()
        {
            return new List<ShoppingBasket>()
            {
                new ShoppingBasket()
                {
                    BasketId = 1,
                    Goods = new List<Product>()
                    {
                        new Product()
                        {
                            Name = "16lb bag of Skittles",
                            Category = "candy",
                            IsImported = false,
                            Number = 1,
                            Price = 16.00m
                        },
                        new Product()
                        {
                            Name = "Walkman",
                            Category = "media players",
                            IsImported = false,
                            Number = 1,
                            Price = 99.99m
                        },
                        new Product()
                        {
                            Name = "Bag of microwave Popcorn",
                            Category = "popcorn",
                            IsImported = false,
                            Number = 1,
                            Price = 0.99m
                        }
                    }
                },
                new ShoppingBasket()
                {
                    BasketId = 2,
                    Goods = new List<Product>()
                    {
                        new Product()
                        {
                            Name = "Imported bag of Vanilla-Hazelnut Coffee",
                            Category = "coffee",
                            IsImported = true,
                            Number = 1,
                            Price = 11.00m
                        },
                        new Product()
                        {
                            Name = "Imported Vespa",
                            Category = "scooters",
                            IsImported = true,
                            Number = 1,
                            Price = 15001.25m
                        }
                    }
                },
                new ShoppingBasket()
                {
                    BasketId = 3,
                    Goods = new List<Product>()
                    {
                        new Product()
                        {
                            Name = "Imported crate of Almond Snickers",
                            Category = "candy",
                            IsImported = true,
                            Number = 1,
                            Price = 75.99m
                        },
                        new Product()
                        {
                            Name = "Discman",
                            Category = "media players",
                            IsImported = false,
                            Number = 1,
                            Price = 55.00m
                        },
                        new Product()
                        {
                            Name = "Imported Bottle of Wine",
                            Category = "wine",
                            IsImported = true,
                            Number = 1,
                            Price = 10.00m
                        },
                        new Product()
                        {
                            Name = "300# bag of Fair-Trade Coffee",
                            Category = "coffee",
                            IsImported = false,
                            Number = 1,
                            Price = 997.99m
                        }
                    }
                }
            };
        }
    }
}
