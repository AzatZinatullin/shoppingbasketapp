using NUnit.Framework;
using ShoppingService.Models;
using ShoppingService.Services;
using System.Collections.Generic;

namespace ShoppingService.Tests
{
    public class ReceiptGeneratorTests
    {
        private static Product product_1 = new Product()
        {
            Name = "IPod",
            Category = "media_players",
            IsImported = false,
            Number = 1,
            Price = 89.99m
        };

        private static Product product_2 = new Product()
        {
            Name = "Snickers box",
            Category = "candy",
            IsImported = true,
            Number = 2,
            Price = 25.50m
        };

        private ShoppingBasket TestShoppingBasket = new ShoppingBasket()
        {
            BasketId = 5,
            Goods = new List<Product>() { product_1, product_2 }
        };

        [Test]
        public void NormalConditions_ExptectedResult_Equals_Actual()
        {
            // Arrange         
            var expectedProduct_1 = new Product();
            var expectedProduct_2 = new Product();

            expectedProduct_1 = product_1;
            expectedProduct_2 = product_2;
            expectedProduct_1.Tax = 9.0m;
            expectedProduct_2.Tax = 1.3m;

            var expectedReceipt = new Receipt();
            expectedReceipt.ReceiptId = 5;
            expectedReceipt.Goods = new List<Product> { expectedProduct_1, expectedProduct_2 };
            expectedReceipt.SalesTaxes = 11.6m;
            expectedReceipt.TotalPrice = 152.59m;

            // Act
            var actualReceipt = ReceiptGenerator.GetReceipt(TestShoppingBasket);

            // Assert
            Assert.AreEqual(expectedReceipt.ReceiptId, actualReceipt.ReceiptId);
            Assert.AreEqual(expectedReceipt.Goods, actualReceipt.Goods);
            Assert.AreEqual(expectedReceipt.SalesTaxes, actualReceipt.SalesTaxes);
            Assert.AreEqual(expectedReceipt.TotalPrice, actualReceipt.TotalPrice);
        }
    }
}