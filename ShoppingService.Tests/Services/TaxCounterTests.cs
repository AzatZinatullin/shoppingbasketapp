using NUnit.Framework;
using ShoppingService.Models;
using ShoppingService.Services;

namespace ShoppingService.Tests
{
    public class TaxCounterTests
    {
        [TestCase(0.00, "candy", false, 15.50)]
        [TestCase(13.50, "media_players", true, 89.99)]
        public void NormalConditions_ExptectedResult_Equals_Actual(decimal expectedResult, 
            string category, bool isImported, decimal price)
        {
            // Arrange         
            var product = new Product()
            {
                Category = category,
                IsImported = isImported,
                Price = price
            };

            // Act
            var actualResult = TaxCounter.CountProductTax(product);

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}